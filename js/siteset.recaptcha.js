/**
 *
 *
 */

var ssRecaptchaVariable = {};

/**
 *
 *
 */

function recaptchaCallback()
{
	$('.ss-recaptcha').each(function () {
		var slug	= $(this).attr('data-ss-recaptcha-slug');
		var key		= $(this).attr('data-ss-recaptcha-key');

		ssRecaptchaVariable[slug] = grecaptcha.render($(this)[0], {
			'sitekey': key
		});
	});
}