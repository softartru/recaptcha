<?php

namespace Siteset\Recaptcha;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
	/**
	 *
	 * @return void
	 */

	public function boot() //: void
	{
		// загружаем локаль
		$this->loadTranslationsFrom(__DIR__ . '/../lang', 'recaptcha');
		// загружаем шаблон
		$this->loadViewsFrom(__DIR__ . '/../views/', 'recaptcha');

		// валидация капчи
		\Validator::extend('recaptcha', function($attribute, $value) {
			return Facade::verify($value);
		}, trans('recaptcha::validation.recaptcha'));
	}

	/**
	 *
	 * @return void
	 */

	public function register() //: void
	{
		// смешиваем конфиги
		$this->mergeConfigFrom(__DIR__ . '/../config/recaptcha.php', 'recaptcha');

		// биндим синглтон
		$this->app->bind('recaptcha', function () {
			return new Handler();
		});

		// создаем поле g-recaptcha-response при пост запросе
		// в случае если его нет
		if (
				request()->isMethod('post')
			&&	!request()->has('g-recaptcha-response')
		)
			request()->merge([
				'g-recaptcha-response' => null,
			]);
	}
}
