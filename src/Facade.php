<?php 

namespace Siteset\Recaptcha;

class Facade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return Handler::class;
    }
}
