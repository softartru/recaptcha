<?php

namespace Siteset\Recaptcha;

class Handler
{
	/**
	 * url api сервиса
	 *
	 * @var string
	 */

	private $url = 'https://www.google.com/recaptcha/api/siteverify';

	/**
	 * ответ сервера
	 *
	 * @var array
	 */

	public $answer = [];

	/**
	 * метод проверки
	 *
	 * @return boolean
	 *
	 * @param string $token
	 */

	public function verify($token)
	{
		// если капча отключена, или юзер авторизован
		if (!env('RECAPTCHA_ACTIVE', false) || !\Auth::guest())
			// возвращаем тру без проверок
			return true;

		// чистим от http(s)://
		$url  = preg_replace('/^http[s]{0,1}\:\/\//', '', $this->url);

		// отделям хост от пути
		preg_match('/^(.*?)(\/.*?)$/', $url, $arr);

		$host = $arr[1];
		$path = $arr[2];

		// открываем сокет
		$answer		= '';
		$socket 	= fsockopen('ssl://' . $host, 443, $errno, $errstr, 30) or die("[".$errno."] " . $errstr);

		socket_set_blocking($socket, TRUE);

		$data = implode('&', [
			'secret='	. urlencode(config('recaptcha.secret')),
			'response='	. urlencode($token),
		]);

		$post = ""
			. "POST ". $path ." HTTP/1.0\r\n"
			. "Host: " . $host . "\r\n"
			. "Content-Type: application/x-www-form-urlencoded; charset=UTF-8\r\n"
			. "Content-Length: " . mb_strlen($data) . "\r\n\r\n"
			. $data . "\r\n"
		;

		// отправляем пост запрос
		fwrite($socket, $post);

		// читаем ответ
		while (!feof($socket))
			$answer .= fgets($socket);

		// закрываем сокет
		fclose($socket);

		preg_match('/({.*?})/sui', $answer, $arr);
		$this->answer = json_decode($arr[1], TRUE);

		return $this->answer['success'];
	}

	/**
	 * вывод шаблона инпута рекапчи
	 *
	 * @param string $slug
	 * @param string|null $view
	 */

	public function input($slug, $view = null)
	{
		if (null === $view)
			$view = 'recaptcha::input';

		return view($view, [
			'slug'	=> $slug,
			'key'	=> config('recaptcha.key'),
		])->render();
	}

	/**
	 * вывод шаблона скрипта гугл рекапчи
	 *
	 */

	public function script()
	{
		return view('recaptcha::script')->render();
	}
}
