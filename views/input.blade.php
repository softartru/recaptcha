@if (env('RECAPTCHA_ACTIVE', false) && Auth::guest())
	<div class="ss-recaptcha" data-ss-recaptcha-slug="{{ $slug }}" data-ss-recaptcha-key="{{ $key }}"></div>
	@if ($errors->has('g-recaptcha-response'))
		<div class="invalid-feedback">{{ $errors->first('g-recaptcha-response') }}</div>
	@endif
@endif