@if (env('RECAPTCHA_ACTIVE', false) && Auth::guest())
	<script src="https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit"></script>
@endif