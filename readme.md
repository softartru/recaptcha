# Рекапча

## Установка

Сервис-провайдер

```php
'providers' => [
    ...
    Siteset\Recaptcha\ServiceProvider::class,
    ...
],
```

Фасад

```php
'aliases' => [
    ...
    'Recaptcha' => Siteset\Recaptcha\Facade::class,
    ...
],
```

Компилятор js и css файлов (webpack.mix.js)

```php
mix.scripts([
        ...
        'vendor/siteset/recaptcha/js/siteset.recaptcha.js',
        ...
    ], name.js
);
```

Ключи гугла в .env

```php
RECAPTCHA_ACTIVE=true
RECAPTCHA_SECRET=строка
RECAPTCHA_KEY=строка
```

Скрипт

```php
<head>
    {!! Recaptcha::script() !!}
</head>
```

## Использование

В форме

```php
<form>
    {!! Recaptcha::input('slug') !!}
</form>
```

slug необходим для реализаций несколько рекапч на странице. должен быть уникальным

Валидация

```php
$rules = [
    'g-recaptcha-response' => 'recaptcha',
];
```